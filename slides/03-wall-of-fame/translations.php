<?php
	$translations = array(
		"Voorzitter" => "Chairman",
		"Vice-voorzitter" => "Vice Chairman",
		"Secretaris" => "Secretary",
		"Penningmeester" => "Treasurer",
		"Algemeen lid" => "General Member",
		"Fotograaf" => "Photographer",
		"Commissaris Intern" => "Commissioner of Internal Affairs",
		"Commissaris Extern" => "Commissioner of External Affairs",
		"Ouderejaars lid" => "Senior member",
		"Commissaris Zorgtoeslag" => "Commissioner Healthcare Allowance",
		"Commissaris Kusje" => "Commissioner Boo-boo",
		"Commissaris Verbanddoos" => "Commissioner Bandage box",
		"Huisarts" => "General Practitioner",
		"Huisarts-assistent" => "GP assistant",
		"Commissaris Heethoofd" => "Commissioner Fire Chief",
		"Promohomo" => "Promotion Homosexual"
	);
